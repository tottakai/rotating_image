import 'package:flutter/material.dart';
import './model/model.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(data: [
        ManMyth(
          "Drain the swamp",
          "https://i.amz.mshcdn.com/Mj23SJeMlbYkvLM7ebBsizEsquI=/950x534/filters:quality(90)/https%3A%2F%2Fblueprint-api-production.s3.amazonaws.com%2Fuploads%2Fcard%2Fimage%2F825911%2Faf6991cf-77c1-4702-9e30-05b750134924.jpg",
        ),
        ManMyth(
          "I would give myself a A+",
          "http://static.digg.com/images/bd18fdc662884a17b6240dc7724d3c6f_cd022282f9af4aeeb1047bf364fa5eae_header.jpeg",
        ),
        ManMyth(
          "I'll probably will do it, maybe definitely",
          "https://www.armytimes.com/resizer/p-hloaJ3u7wxQoDqvy5WXH7DZeU=/1200x0/filters:quality(100)/arc-anglerfish-arc2-prod-mco.s3.amazonaws.com/public/G34J23ESEZHSNEMTXC3VRQYNCQ.jpg",
        )
      ]),
    );
  }
}

class MyHomePage extends StatefulWidget {
  final List<ManMyth> data;

  MyHomePage({Key key, this.data}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _index = 0;

  void _nextPicture() {
    setState(() {
      _index = ++_index % widget.data.length;
    });
  }

  Widget _buildImageRotator() {
    final myth = widget.data[_index];

    return Text("PUT YOUR LAYOUT HERE");
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        title: Text("images"),
      ),
      body: _buildImageRotator(),
    );
  }
}
